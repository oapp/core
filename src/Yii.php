<?php

$yiiPath = __DIR__ . '/../../../yiisoft/yii2';
require "$yiiPath/BaseYii.php";

/**
 * @inheritdoc
 * @property oapp\common\components\web\Application|oapp\common\components\console\Application $app
 */
class Yii extends \yii\BaseYii
{
}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap = require __DIR__ . '/classes.php';
Yii::$container = new yii\di\Container();
